# [1.9.0](https://gitlab.com/draftup/utility.js/compare/v1.8.1...v1.9.0) (2023-04-13)

### Bug Fixes

- cache ([06c36fc](https://gitlab.com/draftup/utility.js/commit/06c36fc53ba7df218f1139a78415468f1b432eaa))

### Features

- cache ([9de74e4](https://gitlab.com/draftup/utility.js/commit/9de74e4b6e38b1df931c113b9f96a0e01b34f7d5))
- store ([e91819b](https://gitlab.com/draftup/utility.js/commit/e91819bb8c52af30441d12926f2920a75e2883ff))

## [1.8.1](https://gitlab.com/draftup/utility.js/compare/v1.8.0...v1.8.1) (2023-04-13)

### Bug Fixes

- store, cache ([7581495](https://gitlab.com/draftup/utility.js/commit/7581495ddb7332496d6b180ac327fa914db3ced7))

# [1.8.0](https://gitlab.com/draftup/utility.js/compare/v1.7.0...v1.8.0) (2023-04-10)

### Features

- dictionary cache ([a356b88](https://gitlab.com/draftup/utility.js/commit/a356b88144ba92655a83cf435fdc85e259dae1b0))
- dictionary store utility ([45a7862](https://gitlab.com/draftup/utility.js/commit/45a7862fb7011bc9712cce85e0024c05ea0a42c2))

# [1.7.0](https://gitlab.com/draftup/utility.js/compare/v1.6.0...v1.7.0) (2023-01-26)

### Features

- cache now extends store ([7444385](https://gitlab.com/draftup/utility.js/commit/744438537fd779090246826993079f0d3e5ed573))

# [1.6.0](https://gitlab.com/draftup/utility.js/compare/v1.5.1...v1.6.0) (2022-07-08)

### Features

- store utility ([13c840b](https://gitlab.com/draftup/utility.js/commit/13c840b92e7311cb473ea68541c5e38e1cc03e95))

## [1.5.1](https://gitlab.com/draftup/utility.js/compare/v1.5.0...v1.5.1) (2022-07-08)

### Bug Fixes

- **dictionary cache:** inaccurate readValue return type ([0fa6b27](https://gitlab.com/draftup/utility.js/commit/0fa6b278645af6294ddd93ca0cb2a2fce7ddd005))

# [1.5.0](https://gitlab.com/draftup/utility.js/compare/v1.4.0...v1.5.0) (2022-06-07)

### Features

- obaservable utility ([05dc422](https://gitlab.com/draftup/utility.js/commit/05dc422b8ca4a1a5c85355b60dada3618eefcf76))

# [1.4.0](https://gitlab.com/draftup/utility.js/compare/v1.3.3...v1.4.0) (2022-05-03)

### Features

- **dictionary cache:** ability to delete entry ([6392e30](https://gitlab.com/draftup/utility.js/commit/6392e30b2dc7495d526254244725fa820928c9cb))

## [1.3.3](https://gitlab.com/draftup/utility.js/compare/v1.3.2...v1.3.3) (2022-05-02)

### Bug Fixes

- npmignore ([1684f61](https://gitlab.com/draftup/utility.js/commit/1684f61ee1d5aceb59edaa636e86b592d9547926))

## [1.3.2](https://gitlab.com/draftup/utility.js/compare/v1.3.1...v1.3.2) (2022-05-02)

### Bug Fixes

- flow type definitions ([f382c6f](https://gitlab.com/draftup/utility.js/commit/f382c6f83101c2dbed614ebafe27616c058d7b13))
- missing exports ([2659ca5](https://gitlab.com/draftup/utility.js/commit/2659ca50051ea170f4dd6f32afe496ea9927e474))
- more strict typings for async process stage names ([b5c0763](https://gitlab.com/draftup/utility.js/commit/b5c07638b18f3d618f1b9baeb8ae8003348faad8))

### Reverts

- replace typescript with flow ([5294aa6](https://gitlab.com/draftup/utility.js/commit/5294aa68e55ddfa58da237adb3d7428065d9433e))

## [1.3.1](https://gitlab.com/draftup/utility.js/compare/v1.3.0...v1.3.1) (2022-05-01)

### Bug Fixes

- replace typescript with flow ([e9cbaa2](https://gitlab.com/draftup/utility.js/commit/e9cbaa23e3f4830790bb910607f73734361898bf))

# [1.3.0](https://gitlab.com/draftup/utility.js/compare/v1.2.0...v1.3.0) (2022-05-01)

### Features

- async process utility ([b185e0f](https://gitlab.com/draftup/utility.js/commit/b185e0f637897cb5d48358a97d69d833ccbc33b4))

# [1.2.0](https://gitlab.com/draftup/utility.js/compare/v1.1.0...v1.2.0) (2022-04-26)

### Bug Fixes

- drop cache invalidation on writing value ([3086872](https://gitlab.com/draftup/utility.js/commit/308687213fc29dc68254ce66bc4ba9ba4d674d84))

### Features

- dictionary cache ([3dfba82](https://gitlab.com/draftup/utility.js/commit/3dfba8271b2c789ae39143d0f18408087a64d84b))

# [1.1.0](https://gitlab.com/draftup/utility.js/compare/v1.0.0...v1.1.0) (2022-04-26)

### Features

- cache ([f0ec9c5](https://gitlab.com/draftup/utility.js/commit/f0ec9c52f10139dcdd3452a3c6dadc5ddc176ca8))

# 1.0.0 (2022-04-25)

### Features

- async timeout ([434e40c](https://gitlab.com/draftup/utility.js/commit/434e40c717ef98dee9af1eaf9c7452d8bc8f9112))

# 1.0.0 (2022-04-25)

### Features

- async timeout ([4db9c35](https://gitlab.com/draftup/utility.js/commit/4db9c356b30c88d2e031a27c4fb9ed723d553523))

# 1.0.0 (2022-04-25)

### Features

- async timeout ([cef49f7](https://gitlab.com/draftup/utility.js/commit/cef49f7906c438b54f049612203a7f635a9b2eb8))

## [1.1.1](https://gitlab.com/draftup/utility.js/compare/v1.1.0...v1.1.1) (2022-04-13)

### Bug Fixes

- use default export for class modules ([90c0691](https://gitlab.com/draftup/utility.js/commit/90c069151f0502038aed88ba058dae16ccda69f1))

# [1.1.0](https://gitlab.com/draftup/utility.js/compare/v1.0.0...v1.1.0) (2022-04-13)

### Features

- async timeout as a class ([96f4645](https://gitlab.com/draftup/utility.js/commit/96f4645acbfe6c7334063b08370936066d835e6c))

# 1.0.0 (2021-04-18)

### Features

- async timeout utils ([c78367d](https://gitlab.com/draftup/utility.js/commit/c78367d1788bbaae8163c4a1daa83ed83a430a2c))
