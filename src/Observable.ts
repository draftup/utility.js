type Callback = () => void;

export default class Observable {
  private callbacks: Callback[] = [];

  subscribe(callback: Callback): () => void {
    this.callbacks.push(callback);

    const unsubscribe = () => {
      this.callbacks = this.callbacks.filter(
        (currentCallback) => currentCallback !== callback
      );
    };

    return unsubscribe;
  }

  notify() {
    this.callbacks.forEach((callback) => {
      callback();
    });
  }
}
