import Observable from "./Observable";

type SetterCallback<ValueType> = (prevValue: ValueType) => ValueType;

export default class DictionaryStore<KeyType, ValueType> extends Observable {
  private values: Map<KeyType, ValueType>;

  constructor(
    initialValue:
      | DictionaryStore<KeyType, ValueType>
      | Iterable<[KeyType, ValueType]> = []
  ) {
    super();

    if (initialValue instanceof DictionaryStore) {
      this.values = new Map(initialValue.getEntries());
    } else {
      this.values = new Map(initialValue);
    }
  }

  setValue(
    key: KeyType,
    nextValue: ValueType | SetterCallback<ValueType>
  ): DictionaryStore<KeyType, ValueType> {
    if (nextValue instanceof Function) {
      this.values.set(key, nextValue(this.values.get(key)));
    } else {
      this.values.set(key, nextValue);
    }

    this.notify();

    return this;
  }

  deleteValue(key: KeyType): DictionaryStore<KeyType, ValueType> {
    this.values.delete(key);

    this.notify();

    return this;
  }

  getValue(key: KeyType): void | ValueType {
    return this.values.get(key);
  }

  getValues(): Iterable<ValueType> {
    return this.values.values();
  }

  getKeys(): Iterable<KeyType> {
    return this.values.keys();
  }

  getEntries(): Iterable<[KeyType, ValueType]> {
    return this.values.entries();
  }
}
