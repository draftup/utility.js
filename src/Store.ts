import Observable from "./Observable";

type SetterCallback<ValueType> = (prevValue: void | ValueType) => ValueType;

export default class Store<ValueType> extends Observable {
  private value: void | ValueType;

  constructor(initialValue?: Store<ValueType> | ValueType) {
    super();

    if (initialValue instanceof Store) {
      this.value = initialValue.getValue();
    } else {
      this.value = initialValue;
    }
  }

  setValue(nextValue: ValueType | SetterCallback<ValueType>): Store<ValueType> {
    if (nextValue instanceof Function) {
      this.value = nextValue(this.value);
    } else {
      this.value = nextValue;
    }

    this.notify();

    return this;
  }

  deleteValue(): Store<ValueType> {
    this.value = undefined;

    this.notify();

    return this;
  }

  getValue(): void | ValueType {
    return this.value;
  }
}
