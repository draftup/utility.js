import DictionaryStore from "./DictionaryStore";

export default class DictionaryCache<
  KeyType,
  ValueType
> extends DictionaryStore<KeyType, ValueType> {
  private invalidatedKeys: Set<KeyType> = new Set();

  constructor(
    initalEntries?:
      | DictionaryCache<KeyType, ValueType>
      | Iterable<[KeyType, ValueType]>
  ) {
    super(initalEntries);

    if (initalEntries instanceof DictionaryCache) {
      for (let key of initalEntries.getInvalidatedValuesKeys()) {
        this.invalidatedKeys.add(key);
      }
    }
  }

  invalidate(key: KeyType): DictionaryCache<KeyType, ValueType> {
    this.invalidatedKeys.add(key);

    return this;
  }

  isInvalidated(key: KeyType): boolean {
    return this.invalidatedKeys.has(key);
  }

  getInvalidatedValuesKeys(): Iterable<KeyType> {
    return this.invalidatedKeys.keys();
  }

  /**
   * @deprecated Use invalidate instead
   */
  invalidateKey(key: KeyType): DictionaryCache<KeyType, ValueType> {
    this.invalidatedKeys.add(key);

    return this;
  }

  /**
   * @deprecated Use isInvalidated instead
   */
  isKeyInvalidated(key: KeyType): boolean {
    return this.invalidatedKeys.has(key);
  }

  /**
   * @deprecated Use getInvalidatedValuesKeys instead
   */
  readAllInvalidatedKeys(): KeyType[] {
    return Array.from(this.invalidatedKeys.keys());
  }

  updateRecord(
    key: KeyType,
    nextValue: ValueType
  ): DictionaryCache<KeyType, ValueType> {
    this.setValue(key, nextValue);

    this.invalidatedKeys.delete(key);

    return this;
  }

  deleteRecord(key: KeyType): DictionaryCache<KeyType, ValueType> {
    this.invalidatedKeys.delete(key);

    this.deleteValue(key);

    return this;
  }

  /**
   * @deprecated Use updateEntry instead
   */
  writeValue(
    key: KeyType,
    nextValue: ValueType
  ): DictionaryCache<KeyType, ValueType> {
    this.setValue(key, nextValue);

    this.invalidatedKeys.delete(key);

    return this;
  }

  /**
   * @deprecated Use getValue instead
   */
  readValue(key: KeyType): ValueType | void {
    return this.getValue(key);
  }

  /**
   * @deprecated Use getEntries instead
   */
  readEntries(): [KeyType, ValueType][] {
    return Array.from(this.getEntries());
  }

  /**
   * @deprecated Use deleteRecord instead
   */
  deleteEntry(key: KeyType): DictionaryCache<KeyType, ValueType> {
    this.invalidatedKeys.delete(key);

    this.deleteValue(key);

    return this;
  }
}
