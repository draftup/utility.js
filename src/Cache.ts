import Store from "./Store";

export default class Cache<ValueType> extends Store<ValueType> {
  private invalidated: boolean = false;

  constructor(initalValue?: Cache<ValueType> | ValueType) {
    super(initalValue);

    if (initalValue instanceof Cache) {
      this.invalidated = initalValue.isInvalidated();
    }
  }

  invalidate(): Cache<ValueType> {
    this.invalidated = true;

    this.notify();

    return this;
  }

  isInvalidated(): boolean {
    return this.invalidated;
  }

  updateRecord(nextValue: ValueType): Cache<ValueType> {
    this.invalidated = false;

    this.setValue(nextValue);

    return this;
  }

  deleteRecord(): Cache<ValueType> {
    this.invalidated = false;

    this.deleteValue();

    return this;
  }

  /**
   * @deprecated Use updateRecord instead
   */
  updateValue(nextValue: ValueType): Cache<ValueType> {
    this.invalidated = false;

    this.setValue(nextValue);

    return this;
  }

  /**
   * @deprecated Use updateValue instead
   */
  writeValue(nextValue: ValueType): Cache<ValueType> {
    return this.updateValue(nextValue);
  }

  /**
   * @deprecated Use getValue instead
   */
  readValue(): ValueType | void {
    return this.getValue();
  }
}
