export default class AsyncTimeout {
  private timeoutMS: number = 0;

  constructor(timeoutMS: number) {
    this.timeoutMS = timeoutMS;
  }

  async start(): Promise<void> {
    return new Promise((resolve) => {
      setTimeout(resolve, this.timeoutMS);
    });
  }
}
