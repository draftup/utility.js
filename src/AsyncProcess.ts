// Errors

class AsyncProcessError extends Error {
  name: string = "Async Process Error";
}

// Stages

type IdleStageName = "IDLE";

class IdleStage {
  name: IdleStageName = IdleStage.NAME;

  static NAME: IdleStageName = "IDLE";
}

type PendingStageName = "PENDING";

class PendingStage {
  name: PendingStageName = PendingStage.NAME;

  static NAME: PendingStageName = "PENDING";
}

type ResolvedStageName = "RESOLVED";

class ResolvedStage<ResultType> {
  name: ResolvedStageName = ResolvedStage.NAME;

  result: ResultType;

  constructor(result: ResultType) {
    this.result = result;
  }

  static NAME: ResolvedStageName = "RESOLVED";
}

type RejectedStageName = "REJECTED";

class RejectedStage {
  name: RejectedStageName = RejectedStage.NAME;

  error: Error;

  constructor(error: Error) {
    this.error = error;
  }

  static NAME: RejectedStageName = "REJECTED";
}

// Implementation

type AsyncProcessStage<ResultType> =
  | IdleStage
  | PendingStage
  | ResolvedStage<ResultType>
  | RejectedStage;

export default class AsyncProcess<ResultType> {
  stage: AsyncProcessStage<ResultType>;

  constructor(asyncProcess?: AsyncProcess<ResultType>) {
    if (asyncProcess instanceof AsyncProcess) {
      this.stage = asyncProcess.stage;
    } else {
      this.stage = new IdleStage();
    }
  }

  pend(): AsyncProcess<ResultType> {
    if (this.stage instanceof IdleStage) {
      this.stage = new PendingStage();

      return this;
    }

    throw new AsyncProcessError(
      `Illegal transaction: ${this.stage.name} > ${PendingStage.NAME}`
    );
  }

  resolve(result: ResultType): AsyncProcess<ResultType> {
    if (this.stage instanceof PendingStage) {
      this.stage = new ResolvedStage(result);

      return this;
    }

    throw new AsyncProcessError(
      `Illegal transaction: ${this.stage.name} > ${ResolvedStage.NAME}`
    );
  }

  reject(error: Error): AsyncProcess<ResultType> {
    if (this.stage instanceof PendingStage) {
      this.stage = new RejectedStage(error);

      return this;
    }

    throw new AsyncProcessError(
      `Illegal transaction: ${this.stage.name} > ${RejectedStage.NAME}`
    );
  }

  static IDLE_STAGE_CONSTRUCTOR = IdleStage;

  static PENDING_STAGE_CONSTRUCTOR = PendingStage;

  static RESOLVED_STAGE_CONSTRUCTOR = ResolvedStage;

  static REJECTED_STAGE_CONSTRUCTOR = RejectedStage;
}
